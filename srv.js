#!/usr/bin/env node
const path = process.cwd();
const nStatic = require('node-static');
const fileServer = new nStatic.Server(path);
const http = require('http');

const srv = http.createServer((request, response)=>
	request.addListener('end', ()=>fileServer.serve(request, response) ).resume()
);
srv.listen(process.argv[2], ()=> {
	console.log('CV here : http://localhost:'+srv.address().port+'/');
//	require("opn")("http://localhost:"+srv.address().port+"/");
});
